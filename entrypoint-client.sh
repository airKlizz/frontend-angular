#!/bin/sh
set -e
if grep -q '$CMS_BASE_URL' /usr/share/nginx/html/assets/config.local.json; then

envsubst < /usr/share/nginx/html/assets/config.local.json > /usr/share/nginx/html/assets/config.local.json.temp
mv -f /usr/share/nginx/html/assets/config.local.json.temp /usr/share/nginx/html/assets/config.local.json
#nginx -s reload

fi
exec "$@"