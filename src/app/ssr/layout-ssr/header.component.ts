import { ChangeDetectorRef, Component, OnDestroy, Inject, Injector, OnInit, PLATFORM_ID, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser } from "@angular/common";
import { combineLatest, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { BaseComponent } from '../../framework/core';
import { Store } from '@ngrx/store';

import * as MenuAction from '../../store/menu/menu.actions';
import { CmsMenuTree, CmsMenuTreeItem, CmsSubmenuTree, Menu } from '../../library/menu/menu.model';
import { CmsMenuService } from '../../+cms/cms-menu.service';
import { MenuState } from '../../store/menu/menu.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['header.component.scss'],
  providers: [CmsMenuService]
  // TODO: maintain immutability
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent extends BaseComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  baseUrl: string;
  title: string;
  siteMenuItems: Array<CmsMenuTreeItem>;
  externalMenuAvailable: boolean;
  menu: Observable<CmsMenuTree>;
  cmsMenu: CmsMenuTree;
  acitveMenuItem: string;
  acitveMenuSubItem: string;
  acitveMenuLVL3Item: string;
  submenus: Array<CmsSubmenuTree>;
  hasSubmenus = false;
  hasLVL3Submenus = false;
  currentSubmenu: Array<CmsMenuTreeItem>;
  currentSubmenuLVL3: Array<CmsMenuTreeItem>;
  isSubmenu = false;
  parentLink = '';
  isAuthenticated = false; // TODO: access only through getter
  isBrowser = isPlatformBrowser(this.platformId);
  userDetails: any;
  idToken: any;
  showDashboard = false;
  private readonly sub: any;
  private readonly menuSub: Subscription;
  private readonly routeSub: Subscription;


  constructor(
    private readonly menuStore: Store<MenuState>,
    private readonly cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private readonly platformId: any,
  ) {
    super();
  }

  async ngOnInit():Promise<void> {
    this.title = 'APP_NAME';
    if (this.isBrowser) {
      this.isAuthenticated = false;
    }

    // this.menuStore.dispatch(new MenuAction.GetMenu('main'));

    combineLatest([
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).menu ),
        distinctUntilChanged()
      ),
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).currentRoute ),
        distinctUntilChanged()
      )]
    ).subscribe(([menuData, curRoute]) => {
      this.siteMenuItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;
      // this.drawSubmenu(this.siteMenuItems, curRoute);

      this.externalMenuAvailable = true;

      this.cd.markForCheck();
    });
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  async logout(): Promise<void> {
    if (this.isBrowser) {

    }
  }

  async login(): Promise<void> {
    if (this.isBrowser) {

    }
  }

  navigateToDashboard($event: Event):void {
    $event.preventDefault();

    if(this.isBrowser){
      window.location.href = `/catalogue/mygrid/`;
    }
}

}
