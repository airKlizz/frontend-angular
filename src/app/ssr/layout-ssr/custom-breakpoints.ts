import {BREAKPOINT} from '@angular/flex-layout';

const PRINT_BREAKPOINTS = [{
  alias: 'sd',
  suffix: 'sdPoint',
  mediaQuery: '@media (min-width: 768px)',
  overlapping: false,
  priority: 1001 // Needed if overriding the default print breakpoint
}];

export const CustomBreakPointsProvider = { 
  provide: BREAKPOINT,
  useValue: PRINT_BREAKPOINTS,
  multi: true
};