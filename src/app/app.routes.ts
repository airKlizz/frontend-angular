import { ChangeLanguageComponent } from './framework/i18n';
import { AppAuthGuard } from './auth-guard';
import { CheckRouteComponent } from './check-route/check-route.component';
import { CmsComponent } from './+cms/cms.component';


export const routes = [
      {
        path: '',
        component: CmsComponent
      },
      {
        path: 'news',
        loadChildren: async () => import('./news/news.module').then(m => m.NewsModule )
      },
      {
        path: 'ui-showdown',
        loadChildren: async () => import('./+showdown/showdown.module').then(m => m.ShowdownModule )
      },
      {
        path: 'profile',
        canActivate: [AppAuthGuard],
        loadChildren: async () => import('./+profile/profile.module').then(m =>  m.ProfileModule )
      },
      {
        path: 'silo',
        loadChildren: async () => import('./+silo/silo.module').then(m =>  m.SiloModule )
      },
      {
        path: 'contact-us',
        loadChildren: async () => import('./+contactform/contact.module').then(m =>  m.ContactModule )
      },
      {
        path: 'request-provider-role',
        canActivate: [AppAuthGuard],
        loadChildren: async () => import('./request-provider/request-provider.module').then(m => m.RequestProviderRoleModule)
      },
      {
        path: 'check-route', component: CheckRouteComponent
      },

  {
    path: 'change-language/:languageCode',
    component: ChangeLanguageComponent
  },
  {
    path: '**',
    redirectTo: 'check-route',
    pathMatch: 'full'
  }
];
