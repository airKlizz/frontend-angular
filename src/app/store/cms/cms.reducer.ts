/* eslint-disable prefer-arrow/prefer-arrow-functions */
import * as CmsActions from './cms.actions';
import { CmsState } from './cms.state';

const defaultCmsState: CmsState = {
  serverData: null,
  newsData: null,
  savedPageSlug : null,
  catalogueStats: null,
  loading: false
};

// tslint:disable-next-line:only-arrow-functions
export function CmsReducer(state: CmsState = defaultCmsState, action: any): CmsState {
  switch (action.type) {
    case CmsActions.CmsActionTypes.GET_CMS_PAGE: {

      return {
        ...state,
        loading: true };
    }
    case CmsActions.CmsActionTypes.SET_CMS_SERVER_PAGE: {
      // console.log("action", action);

      return {
        ...state,
        serverData: action.cmsData,
        savedPageSlug : action.cmsData.path[0].alias,
        loading: false };
    }

    case CmsActions.CmsActionTypes.GET_NEWS_PAGE_SUCCESS: {
      // console.log("news", action.payload);

      return {
        ...state,
        newsData: action.payload
       };
    }

    case CmsActions.CmsActionTypes.GET_CATALOGUE_STATISTICS_SUCCESS: {
      const resBucket = action.payload.facets._filter_resource_type.resource_type.buckets;
      const typeBucket = action.payload.facets._filter_entity_type.entity_type.buckets;

      return {
        ...state,
        catalogueStats: {
          corpus: resBucket.find(bucket => bucket["key"] === "Corpus").doc_count,
          toolsService: resBucket.find(bucket => bucket["key"] === "Tool/Service").doc_count,
          conceptual: resBucket.find(bucket => bucket["key"] === "Lexical/Conceptual resource").doc_count,
          modelsGrammar: parseInt(resBucket.find(bucket => bucket["key"] === "Model").doc_count, 10) + parseInt(resBucket.find(bucket => bucket["key"] === "Grammar").doc_count, 10) + parseInt(resBucket.find(bucket => bucket["key"] === "Uncategorized Language Description").doc_count, 10),
          organization: typeBucket.find(bucket => bucket["key"] === "Organization").doc_count,
          projects: typeBucket.find(bucket => bucket["key"] === "Project").doc_count
        }
       };
    }

    default:
      return state;
  }
}
