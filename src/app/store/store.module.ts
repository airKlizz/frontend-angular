import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MetaReducer, StoreModule as NgrxStoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreFrameworkModule } from '../framework/store';
import { environment } from '../../environments/environment';

import { MenuModule } from './menu/menu.module';
import { WebformModule } from './webforms/webform.module';
import { SearchResultsModule } from './search/search.module';
import { AuthModule } from './auth/auth.module'
import { CmsStateModule } from './cms/cms.module';

@NgModule({
  imports: [
    CommonModule,
    NgrxStoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    !environment.production && environment.hasStoreDevTools ? StoreDevtoolsModule.instrument() : [],
    StoreFrameworkModule.forRoot(),
    MenuModule,
    WebformModule,
    SearchResultsModule,
    AuthModule,
    CmsStateModule
  ]
})
export class StoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: StoreModule) {
    if (parentModule) {
      throw new Error('StoreModule already loaded. Import in root module only.');
    }
  }
  static forRoot(): ModuleWithProviders<StoreModule> {
    return {
      ngModule: StoreModule
    };
  }


}
