export interface AuthState {
  isAuthenticated: boolean;
  idToken: any,
  userProfile: any
}
