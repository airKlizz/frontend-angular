import { Action } from '@ngrx/store';

import { Auth } from '../../library/auth/auth.model';

export enum AuthTypes {
  SET_LOGIN_STATE = '[Auth] SET_LOGIN_STATE',
  SET_ID_TOKEN = '[Auth] SET_ID_TOKEN',
  SET_USER_PROFILE = '[Auth] SET_USER_PROFILE'
}

export class SetAuthState implements Action {
  readonly type = AuthTypes.SET_LOGIN_STATE;

  constructor(public isLoggedIn: boolean) {}
}

export class SetIDToken implements Action {
  readonly type = AuthTypes.SET_ID_TOKEN;

  constructor(public idToken: any) {}
}

export class SetUserProfile implements Action {
  readonly type = AuthTypes.SET_USER_PROFILE;

  constructor(public userProfile: any) {}
}

export type Actions =
  | SetAuthState
  | SetIDToken
  | SetUserProfile;
