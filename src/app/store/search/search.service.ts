import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from '@ngx-config/core';
import { EMPTY, Observable, of as observableOf } from 'rxjs';
import { delay, map, retry } from 'rxjs/operators';
import { BaseEntityService, HTTP_CLIENT__MAX_RETRIES, UniqueId } from '~/app/framework/ngrx';

// import { searchJSON } from '../../../assets/data/demo-search-result.json';
import { SearchResults } from '../../library/search/models/search-results.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  delay: number;

  constructor(protected readonly config: ConfigService, protected readonly http: HttpClient) {
    this.delay = 4000;
  }

  getJSON$(): Observable<SearchResults> {
    return this.http.get<SearchResults>('../../../assets/data/demo-search-result.json');
  }
}
