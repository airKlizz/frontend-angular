import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, OnDestroy, Inject, Injector, PLATFORM_ID, Output, EventEmitter} from '@angular/core';
import { Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { BaseComponent } from './framework/core';
import { languageActions, State } from './store';
import { KeycloakService } from 'keycloak-angular';
import { AuthState} from './store/auth/auth.state';
// import { SetAuthState } from './store/auth/auth.actions';
import * as AuthAction from './store/auth/auth.actions';

import { isPlatformBrowser } from "@angular/common";
import { combineLatest } from 'rxjs';
import { distinctUntilChanged, map, filter } from 'rxjs/operators';

import * as MenuAction from './store/menu/menu.actions';
import { CmsMenuTree, CmsMenuTreeItem } from './library/menu/menu.model';
import { MenuState } from './store/menu/menu.state';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./layout/main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent extends BaseComponent implements OnInit {
  isAuthenticated = false;
  isBrowser = isPlatformBrowser(this.platformId);
  userDetails: any;
  siteMenuItems: Array<CmsMenuTreeItem>;
  feedbackItems: Array<CmsMenuTreeItem>;
  externalMenuAvailable: boolean;

  constructor(
    private readonly store$: Store<State>,
    private readonly authStore: Store<AuthState>,
    private readonly config: ConfigService,
    private readonly keycloakService: KeycloakService,
    private readonly menuStore: Store<MenuState>,
    private readonly cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private readonly platformId: any,
  ) {
    super();
  }

  ngOnInit(): void {

    const settings = this.config.getSettings('i18n');
    this.store$.dispatch(languageActions.i18nInitLanguage(settings));

    this.keycloakService.isLoggedIn().then((status) => {

      this.authStore.dispatch(new AuthAction.SetAuthState(status));

    }).catch((err)=>{
      console.log(err);
    });

    this.keycloakService.loadUserProfile().then((profile) => {
      this.authStore.dispatch(new AuthAction.SetUserProfile(profile));
    }).catch((err)=>{
      console.log(err);
    });


      this.authStore.dispatch(new AuthAction.SetIDToken(this.keycloakService.getKeycloakInstance().idTokenParsed));










    if (this.isBrowser) {

      this.keycloakService.isLoggedIn().then(async (answer) => {
        if( answer ){
            this.isAuthenticated = true;
            this.userDetails = await this.keycloakService.loadUserProfile();
            this.cd.markForCheck();
        } else {
          this.isAuthenticated = false;
        }

      }).catch((error) => {
        console.log(error);
      });


    }

    this.menuStore.dispatch(new MenuAction.GetMenu('main'));

    combineLatest([
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).menu ),
        distinctUntilChanged()
      ),
      this.menuStore.select('menu').pipe(
        map(menuTree => ((menuTree as unknown) as MenuState).currentRoute ),
        distinctUntilChanged()
      )
    ]
    ).pipe(
      filter(([menuData, curRoute]) =>  menuData !== undefined)
    ).subscribe(([menuData, curRoute]) => {
      // console.log('menuData: ', menuData, curRoute);
      this.siteMenuItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;

      this.externalMenuAvailable = true;

      this.cd.markForCheck();
    });


    // this.menuStore.dispatch(new MenuAction.GetFeedbackMenu('feedback'));

    // this.menuStore
    //   .select('menu')
    //   .pipe(
    //     map(menuTree => ((menuTree as unknown) as MenuState).feedbackMenu),
    //     distinctUntilChanged()
    //   )
    //   .subscribe(menuData => {
    //     this.feedbackItems = (menuData as unknown as CmsMenuTree).menuItems; // .menuItems;

    //     this.cd.markForCheck();
    //   });


  }


  async logout(): Promise<void> {
    if (this.isBrowser) {
      return this.keycloakService.logout();
    }
  }

  async login(): Promise<void> {
    if (this.isBrowser) {
    return this.keycloakService.login();
    }
  }

}
