import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestProviderComponent } from './request-provider.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../framework/core/';
import { MaterialModule } from '../framework/material';
import { ProviderApplicantService } from './provider-applicant.service';
import { CookieService } from 'app/layout/foot/cookie.service';

export const routes = [
  {
    path: '',
    component: RequestProviderComponent,
    data: {
      meta: {
        title: 'Prowider tilte',
        description: 'Provider desc'
      }
    }
  }
];

@NgModule({
  declarations: [RequestProviderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    MaterialModule
  ],
  providers: [ProviderApplicantService, CookieService]
})
export class RequestProviderRoleModule { }
