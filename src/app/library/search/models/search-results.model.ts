export class SearchResults {
  count: number;
  next: string | null;
  previous: SearchResult | null;
  facets: Facet | null;
  results: Array<SearchResult> | [];

  constructor() {
    this.count = 0;
    this.next = undefined;
    this.previous = undefined;
    this.facets = undefined;
    this.results = [];
  }

  static generateMockSearchResults(): SearchResults {
    return {
      count: 0,
      next: undefined,
      previous: undefined,
      facets: undefined,
      results: []
    };
  }


}

export class TopCategories {
  categories: Array<Bucket>;

  constructor() {
    this.categories = [];
  }

  static generateTopCategories(): TopCategories {
    return { categories : [{
      key: '',
      doc_count: 0
    }]};
  }
}

export class Services {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateServices(): Services {
    return { results : []};
  }
}

export class FeaturedToolsAndService {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateFeaturedToolsAndService(): FeaturedToolsAndService {
    return { results : []};
  }
}

export class FeaturedLanguageResource {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateFeaturedToolsAndService(): FeaturedLanguageResource {
    return { results : []};
  }
}

export class FeaturedOrganizations {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateFeaturedOrganizations(): FeaturedOrganizations {
    return { results : []};
  }
}



export class RecentlyUpdatedTech {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateRecentlyUpdatedTech(): RecentlyUpdatedTech {
    return { results : []};
  }
}

export class MostPopularTech {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateMostPopularTech(): MostPopularTech {
    return { results : []};
  }
}

export class SearchResultsCollection {
  results: Array<SearchResult> | [];

  constructor() {
    this.results = [];
  }

  static generateSearchResultsCollection(): SearchResultsCollection {
    return { results : []};
  }
}

export interface Facet {
  _filter_language: FilterLanguage;
  _filter_language_eu?: FilterLanguage;
  _filter_language_eu_other?: FilterLanguage;
  _filter_language_rest?: FilterLanguage;
  _filter_licence: FilterLicence;
  _filter_resource_type: FilterResourceType;

  _filter_intended_application: Filterintendedapplication;
  _filter_entity_type: Filterentitytype;
  _filter_function: Filterfunction;
}

interface Filterintendedapplication {
  doc_count: number;
  intended_application: Language;
}

interface Filterentitytype {
  doc_count: number;
  entity_type: Language;
}

interface Filterfunction {
  doc_count: number;
  function: Language;
}

export interface FilterLicence {
  doc_count: number;
  licence: Licence;
}

export interface Licence {
  doc_count_error_upper_bound: number;
  sum_other_doc_count: number;
  buckets: Array<Bucket>;
}

export interface FilterResourceType {
  doc_count: number;
  resource_type: ResourceType;
}

export interface ResourceType {
  doc_count_error_upper_bound: number;
  sum_other_doc_count: number;
  buckets: Array<Bucket>;
}

export interface FilterLanguage {
  doc_count: number;
  language?: Language;
  language_eu?: Language;
  language_eu_other?: Language;
  language_rest?: Language;
}

export interface Language {
  doc_count_error_upper_bound: number;
  sum_other_doc_count: number;
  buckets: Array<Bucket>;
}

export interface Bucket {
  key: string;
  doc_count: number;
}

export interface SearchResult {
  id: number;
  resource_type: string;
  resource_name: string;
  description: string;
  detail: string;
  licences: Array<string>;
  languages: Array<string>;
  entity_type: string;
  keywords: Array<string>;
  creation_date: string;
  last_date_updated: string;
  follows_elg_specs: boolean;
  functions: Array<string>;
  intended_applications: Array<string>;
  views: number;
  downloads: number;
  size: number;
  service_execution_count: number;
  status: string;
}
