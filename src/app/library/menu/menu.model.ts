export class Menu {
  menuTree: Array<CmsMenuTreeItem>;

  constructor() {
    this.menuTree = [];
  }

  static generateMockMenu(): Array<CmsMenuTreeItem> {
    return [];
  }

  static generateMockFooterMenu(): Array<CmsMenuTreeItem> {
    return [];
  }

  static generateMockFeedbackMenu(): Array<CmsMenuTreeItem> {
    return [];
  }
}

export interface CmsSubmenuTree {
  menuItems: Array<CmsMenuTreeItem>;
  path: string;
}

export interface CmsMenuTree {
  menuItems: Array<CmsMenuTreeItem>;
}

// export interface CmsMenuTreeItem {
//   count: number;
//   depth: number;
//   has_children: boolean;
//   in_active_trail: boolean;
//   link: CmsMenuTreeLink;
//   subtree: Array<CmsMenuTreeItem>;
//   uri: string;
//   below: Array<CmsMenuTreeItem>;
//   breadcrumbs: Array<MenuBreadcrumb>;
//   type: string;
// }

export interface MenuBreadcrumb {
  title: string;
  path: string;
  depth: number;
}

export interface CmsMenuTreeLink {
  deletable: boolean;
  delete_route: string | null;
  description: string;
  edit_route: string | null;
  enabled: boolean;
  expanded: boolean;
  menu_name: string;
  meta_data: Array<string>;
  options: {
    external: boolean;
  };
  parent: string;
  provider: string;
  resettable: boolean;
  route_name: string;
  route_parameters: Array<string>;
  title: string;
  translatable: boolean;
  url: string;
  weight: string;
}

/*
    NEW MENU MODEL
*/

export interface CmsMenuTreeItem {
  key: string;
  title: string;
  description?: string;
  uri: string;
  alias: string;
  external: boolean;
  absolute: string;
  relative: string;
  existing: boolean;
  weight: string;
  expanded: boolean;
  enabled: boolean;
  uuid?: string;
  options: CmsMenuOption | Array<any>;
  field_elg_menu_link_type?: string;
  below?: Array<CmsMenuTreeItem>;
  field_show_as_not_clickable: string;
  active: boolean;
  childActive: boolean;
  tabindex: boolean;
}

export interface CmsMenuOption {
  query: Array<any>;
  fragment: string;
}

