import { animate, group, query, style, transition, trigger } from '@angular/animations';

export const routeAnimation = trigger('routeAnimation', [
  transition('Results => Details', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
    group([
      query(
        ':enter',
        [
          style({ transform: 'translateX(20%)', opacity: 1 }),
          animate('400ms ease-in-out', style({ transform: 'translateX(0%)', opacity: 1 }))
        ],
        { optional: true }
      ),
      query(':leave', [style({ opacity: 1 }), animate('200ms ease-in-out', style({ opacity: 1 }))], { optional: true })
    ])
  ]),
  transition('Details => Results', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' }), { optional: true }),
    group([
      query(':enter', [style({ transform: 'translateX(-100%)' }), animate('400ms ease-in-out', style({ transform: 'translateX(0%)' }))], {
        optional: true
      }),
      query(':leave', [style({ transform: 'translateX(0%)' }), animate('400ms ease-in-out', style({ transform: 'translateX(100%)' }))], {
        optional: true
      })
    ])
  ]),
  transition('* <=> *', [style({ opacity: 1 }), animate('400ms 150ms ease-in-out', style({ opacity: 1 }))])
]);
