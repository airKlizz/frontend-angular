import { NgModule, PLATFORM_ID, APP_INITIALIZER } from '@angular/core';
import { BrowserTransferStateModule, TransferState } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { AuthModule } from '@ngx-auth/core';
import { CACHE } from '@ngx-cache/core';
import { BrowserCacheModule, MemoryCacheService } from '@ngx-cache/platform-browser';
import { AuthTestingModule } from './framework/auth/testing';
import { ConsoleService, CoreModule, WindowService } from './framework/core';
import { ServiceWorkerModule } from '@angular/service-worker';

import { AppComponent } from './app.component';
import { AppModule, REQ_KEY } from './app.module';
import { AppInitService } from './app-init.service';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { Angulartics2Module } from 'angulartics2';
import { CookieService } from './layout/foot/cookie.service';
import { AppAuthGuard } from './auth-guard';
import { RouterModule } from '@angular/router';

const keycloakService = new KeycloakService();


export const initializeKeycloak = (appInitService: AppInitService) => (): Promise<any> => appInitService.Init();


@NgModule({
  imports: [
    AppModule,
    BrowserTransferStateModule,
    BrowserAnimationsModule,
    BrowserCacheModule.forRoot([
      {
        provide: CACHE,
        useClass: (MemoryCacheService),
        deps: [PLATFORM_ID]
      }
    ]),
    AuthModule.forRoot(),
    AuthTestingModule,
    Angulartics2Module.forRoot(),
    CoreModule.forRoot([
      {
        provide: WindowService,
        useFactory: () =>  window
      },
      {
        provide: ConsoleService,
        useFactory: () => console
      }
    ]),
    KeycloakAngularModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: false }),
    RouterModule
  ],
  providers: [
    CookieService,
    {
      provide: REQUEST,
      useFactory: (transferState: TransferState) => transferState.get<any>(REQ_KEY, {}),
      deps: [TransferState]
    },
    {
      provide: KeycloakService,
      useValue: keycloakService
    },
    AppInitService,
    { provide: APP_INITIALIZER, useFactory: initializeKeycloak, deps: [AppInitService, KeycloakService], multi: true },
    AppAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppBrowserModule {}
