/* eslint-disable class-methods-use-this */
import { isPlatformBrowser, Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  AfterViewChecked,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewEncapsulation,
  Renderer2
} from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ConfigService } from '@ngx-config/core';
import { distinctUntilChanged, map, filter } from 'rxjs/operators';
import { WebformElement, WebFormT, WebformError } from '../../library/webforms/webforms.model';
import { routeAnimation } from '../../shared';
import { MenuState } from '../../store/menu/menu.state';
import { State } from '../../store/state';
import { WebformState } from '../../store/webforms/webform.state';
import { CmsState } from '../../store/cms/cms.state';
import * as CmsAction from '../../store/cms/cms.actions';

import { CmsSubmenuTree } from '../../library/menu/menu.model';
import * as MenuAction from '../../store/menu/menu.actions';
import * as WebformAction from '../../store/webforms/webform.actions';
import { CmsPageContent, CmsService } from '../cms.service';
import { DynamicService } from '../dynamiccontent/dynamic.service';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { DynamicServerService } from '../dynamiccontent/dynamic-server.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-page',
  styleUrls: ['./page.component.scss'],
  templateUrl: './page.component.html',
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [routeAnimation]
})
export class PageComponent implements OnDestroy, OnInit, AfterViewChecked {
  isLoading: boolean;
  cmsPageContent: CmsPageContent;
  isBrowser = isPlatformBrowser(this.platformId);
  noInlineFallback = ['tapsana']; // saraksts ar to statisko html lapu identifikatoriem,
  // kurām nav jāpielieto inlineFallback pipe
  sectionParsed: { [section: string]: boolean } = {
    'page-content-section': false
  };

  /**
   * Class list in content that comes from CMS that will be treated as placeholder and replaced with dynamic content
   */
  classesToParse: Array<string> = [
    'elg-link',
    'photo-modal',
    'elg-catalogue-link',
    'elg-link-outlined',
    'elg-link-anchor',
    'elg-button-primary',
    'elg-button-accent',
    'elg-button-alternate',
    'topic-box',
    'topic-box-catalogue',
    'widget-top-categories',
    'widget-recently-updated',
    'widget-most-popular',
    'widget-latest-added',
    'widget-latest-added-resources',
    'widget-recently-updated-resources',
    'widget-latest-added-projects',
    'widget-latest-added-organizations',
    'become-provider-section',
    'become-provider-section-unregistered',
    'become-provider-section-applied'
  ];

  submenus: Array<CmsSubmenuTree>;
  hasSubmenus = false;
  currentSubmenu: CmsSubmenuTree;
  isSubmenu = false;
  parentLink = '';
  webformFields: WebFormT;
  webformElements: Array<WebformElement> = [];
  pageWebformId: string;
  pageWebform: FormGroup;
  pageHasWebform = false;
  pageWebformSubmitting = false;
  pageWebformError: any;
  pageWebformSuccess: any;
  private pageLoading = true;
  private newPageLoading = false;
  private sub: any;
  formDropdownValues: Array<any> = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly config: ConfigService,
    private readonly router: Router,
    private readonly elRef: ElementRef,
    private readonly cmsService: CmsService,
    private readonly dynamicService: DynamicService,
    private readonly dynamicServerService: DynamicServerService,
    private readonly cd: ChangeDetectorRef,
    private readonly menuStore: Store<MenuState>,
    private readonly wfStore: Store<State>,
    private readonly cmsStore: Store<State>,
    private readonly fb: FormBuilder,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly renderer: Renderer2,
    @Inject(PLATFORM_ID) private readonly platformId: any
  ) {
    this.cmsPageContent = new CmsPageContent();
  }

  ngOnInit(): void {
    // console.log("PAGE prevUrl", this);
    this.sub = this.route.paramMap.subscribe(params => {
      // console.log('params: ', params);

      // tslint:disable-next-line:forin
      for (const i in this.sectionParsed) {
        this.sectionParsed[i] = false;
      }

      combineLatest([
        this.cmsStore
          .select((st: State) => st.cms)
          .pipe(
            map(cmsState => ((cmsState as unknown) as CmsState).serverData),
            // filter(cmsState => cmsState !== null),
            distinctUntilChanged()
          ),
        this.cmsStore
          .select((st: State) => st.cms)
          .pipe(
            map(cmsState => ((cmsState as unknown) as CmsState).savedPageSlug),
            distinctUntilChanged()
          )
      ]).subscribe(([cont, rt]) => {
        if (rt !== this.router.url && !this.newPageLoading) {
          this.newPageLoading = true;
          if (this.router.url !== '/check-route') {
            this.cmsStore.dispatch(new CmsAction.GetCmsPage(this.router.url));
          }
        } else {
          this.cmsPageContent.loadString(
            cont,
            this.config.getSettings('system.cms_base_url'),
            this.config.getSettings('system.cms_root_url'),
            this.config.getSettings('system.applicationUrl')
          );
          this.cmsPageContent.loading = false;
          this.isLoading = false;
          this.pageLoading = true;

          // tslint:disable-next-line:forin
          for (const i in this.sectionParsed) {
            this.sectionParsed[i] = false;
          }

          this.titleService.setTitle(this.cmsPageContent.title.replace('[no-display]', ''));

          this.metaService.addTag({ property: 'og:title', content: this.cmsPageContent.title.replace('[no-display]', '') });
          this.metaService.addTag({ property: 'og:description', content: this.cmsPageContent.subtitle });
          this.metaService.addTag({ property: 'og:type', content: 'website' });
          this.metaService.addTag({ property: 'og:url', content: `${this.config.getSettings('system.cms_base_url')}${this.router.url}` });
          this.metaService.addTag({
            property: 'og:image',
            content: `${this.config.getSettings('system.cms_base_url')}/assets/img/elg-logo.jpg`
          });
          this.metaService.addTag({ property: 'og:image:width', content: '1200' });
          this.metaService.addTag({ property: 'og:image:height', content: '628' });

          // twitter
          this.metaService.addTag({ property: 'twitter:title', content: this.cmsPageContent.title.replace('[no-display]', '') });
          this.metaService.addTag({
            property: 'twitter:url',
            content: `${this.config.getSettings('system.cms_base_url')}${this.router.url}`
          });
          this.metaService.addTag({ property: 'twitter:card', content: 'summary_large_image' });
          this.metaService.addTag({
            property: 'twitter:image',
            content: `${this.config.getSettings('system.cms_base_url')}/assets/img/elg-logo.jpg`
          });

          this.metaService.addTag({ name: 'title', content: this.cmsPageContent.title.replace('[no-display]', '') });
          this.metaService.addTag({ name: 'description', content: this.cmsPageContent.subtitle });

          if (cont.webform !== undefined && cont.webform.length > 0) {
            this.pageHasWebform = true;
            cont.webform.forEach(wf => {
              this.wfStore.dispatch(new WebformAction.GetWebformFields(wf.target_id));
              this.wfStore.dispatch(new WebformAction.GetWebformElements(wf.target_id));
              this.pageWebformId = wf.target_id;
            });
          } else {
            this.pageHasWebform = false;
          }

          this.menuStore.dispatch(new MenuAction.SetCurrentRoute(this.router.url));
          this.cd.markForCheck();
        }
      });

      this.wfStore
        .select((st: State) => st.webforms)
        .pipe(
          map(fields => ((fields as unknown) as WebformState).webformFields),
          distinctUntilChanged()
        )
        .subscribe((ff: any) => {
          this.webformFields = ff;

          // tslint:disable-next-line:forin
          for (const key in this.webformFields) {
            const value = this.webformFields[key];
          }

          this.cd.markForCheck();
        });

      this.wfStore
        .select('webforms')
        .pipe(
          map(fields => ((fields as unknown) as WebformState).webformElements),
          distinctUntilChanged()
        )
        .subscribe((ff: any) => {
          this.webformElements = [];

          this.pageWebform = this.fb.group({
            webformElems: this.fb.array([])
          });
          const formElems = this.pageWebform.get('webformElems') as FormArray;

          // tslint:disable-next-line:forin
          for (const key in ff) {
            const value = ff[key];

            if (key.substring(0, 1) !== '#') {
              if (value['#type'] === 'select') {
                this.prepareDropdownValues(value, key);
              }

              this.addFormElement(value);
              if (!value.test_attr) {
                formElems.push(new FormControl());
              }
            }
          }
          this.cd.markForCheck();
          this.cd.detectChanges();
        });

      this.wfStore
        .select('webforms')
        .pipe(
          map(fields => ((fields as unknown) as WebformState).loading),
          distinctUntilChanged()
        )
        .subscribe((ff: any) => {
          this.pageWebformSubmitting = ff;
          this.cd.markForCheck();
        });

      this.wfStore
        .select('webforms')
        .pipe(
          map(fields => ((fields as unknown) as WebformState).error),
          distinctUntilChanged()
        )
        .subscribe((err: WebformError) => {
          if (err.error) {
            for (const [key, value] of Object.entries(err.errorMessage)) {
              this.pageWebformError = {
                element: key,
                errorMessage: value
              };
            }
          } else {
            this.pageWebformError = {
              element: '',
              errorMessage: ''
            };
          }
          this.cd.markForCheck();
        });

      this.wfStore
        .select('webforms')
        .pipe(
          map(fields => ((fields as unknown) as WebformState).success),
          distinctUntilChanged()
        )
        .subscribe((success: any) => {
          this.pageWebformSuccess = success;
          this.resetForm();
          this.cd.markForCheck();
        });
    });

    this.isBrowser = isPlatformBrowser(this.platformId);
  }
  prepareDropdownValues(value: any, selectId: string): void {
    const Details = [];

    for (const key of Object.keys(value['#options'])) {
      const detail = { val: key, title: value['#options'][key] };

      Details.push(detail);
    }
    this.formDropdownValues[selectId] = Details;
  }
  getRoute(event: any): void {
    const goRoute = event.target.getAttribute('data-link');
    if (goRoute) {
      this.router
        .navigate([goRoute])
        .catch(err => {
          console.log(err);
        })
        .then(() => {
          console.log('');
        })
        .catch(() => 'obligatory catch');
    }
  }

  ngAfterViewChecked(): void {
    if (this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      const pageContentParsed = this.dynamicService.parseSection(
        'page-content-section',
        this.sectionParsed,
        this.cmsPageContent.body,
        this.elRef,
        '.page-content-section',
        this.classesToParse
      );
      if (pageContentParsed) {
        this.sectionParsed['page-content-section'] = true;
        this.pageLoading = false;
      } else {
        this.pageLoading = true;
      }

      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
      this.cd.markForCheck();
    } else if (!this.isBrowser && this.pageLoading && this.cmsPageContent.body.length > 0) {
      const pageContentParsed = this.dynamicServerService.parseSection(
        'page-content-section',
        this.sectionParsed,
        this.cmsPageContent.body,
        this.elRef,
        '.page-content-section',
        this.classesToParse
      );
      const targetSection = this.renderer.selectRootElement('#page-content-section', true);
      const d2 = this.renderer.createElement('div');
      this.renderer.setProperty(d2, 'innerHTML', pageContentParsed.html());
      this.renderer.appendChild(targetSection, d2);

      if (pageContentParsed !== false) {
        this.sectionParsed['page-content-section'] = true;
      }
      this.pageLoading = false;
      for (const i in this.sectionParsed) {
        if (!this.sectionParsed[i]) {
          this.pageLoading = true;
        }
      }
    }
  }

  shouldIterate(elem: any): boolean {
    if (Object.prototype.toString.call(elem) === '[object Object]') {
      return true;
    } else {
      return false;
    }
  }

  addFormElement(elem: WebformElement): void {
    this.webformElements.push(elem);
  }

  getElemType(i: number): string {
    return this.webformElements[i]['#type'];
  }

  fromEntries<T>(entries: Array<[keyof T, T[keyof T]]>): T {
    return entries.reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {} as T);
  }

  submitForm(event): void {
    event.preventDefault();
    // eslint-disable-next-line @typescript-eslint/unbound-method
    const formObj = [];
    formObj.push(['webform_id', this.pageWebformId]);
    (this.pageWebform.get('webformElems') as FormArray).controls.forEach((control: FormControl, i: number) => {
      if (this.webformElements[i]['#type'] !== 'webform_actions') {
        formObj.push([this.webformElements[i]['#name'], control.value]);
      }
    });

    this.pageWebformSubmitting = true;
    this.wfStore.dispatch(new WebformAction.PostWebform(this.fromEntries(formObj)));
  }

  resetForm(): void {
    this.pageWebform.reset();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
