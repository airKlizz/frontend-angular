import { CmsComponent } from './cms.component';

export const routes = [
  {
    path: '',
    component: CmsComponent,
    data: {
      meta: {
        title: 'PUBLIC.CMS.PAGE_TITLE',
        override: true,
        description: 'PUBLIC.CMS.META_DESCRIPTION'
      }
    }
  },
  // { path: '', component: FrontpageComponent },
  // { path: 'home', redirectTo: '', pathMatch: 'full' },
  // { path: 'page/:alias', component: PageComponent },
];