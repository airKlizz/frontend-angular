import { ElementRef, Injectable } from '@angular/core';

import { DomService } from '../dom.service';
import { DynamicServerComponent } from './dynamic-server.component';
import { InteractiveData } from './interactive.model';

// eslint-disable-next-line @typescript-eslint/no-require-imports
const cheerio = require('cheerio');

@Injectable()
export class DynamicServerService {
  constructor(private readonly domService: DomService) {}

  static parseHtml(sectionIdToParse: string, cmsPageContent: Array<string>): any {
    cmsPageContent.push('</section>');
    cmsPageContent.unshift(`<section id='${sectionIdToParse}' class='${sectionIdToParse}'>`);
    const htmlDoc = cheerio.load(cmsPageContent.join(''));
    cmsPageContent.pop();
    cmsPageContent.shift();

    const newMultimediaSec = htmlDoc(`#${sectionIdToParse}`);

    if (newMultimediaSec !== undefined) {
      return newMultimediaSec;
    }
  }

  // Funkcija lapas sadaļu parsēšanai, ievieš dinamisko saturu
  parseSection(
    sectionName: string,
    sectionParsed: { [section: string]: boolean },
    cmsPageContent: Array<string>,
    elRef: ElementRef,
    appendSelector: string,
    classes: Array<string>
  ): any {
    if (!sectionParsed[sectionName]) {
      let pHtml = DynamicServerService.parseHtml(sectionName, cmsPageContent); // parse data
      if (pHtml === undefined) {
        return false;
      }

      for (const i of classes) {
        const tempHTML = this.activateHtml(pHtml.html(), i, DynamicServerComponent, i);
        pHtml = tempHTML;
      }

      return pHtml;
    }

    return false;
  }

  activateHtml(sectionToParse: any, className: string, componentName: any, elementType: string): any {
    const $ = cheerio.load(sectionToParse);
    const inlineElems = $(`.${className}`); // sectionToParse.getElementsByClassName(className);

    if (inlineElems.length > 0) {
      $(inlineElems).each(index => {
        const elemData: InteractiveData = {
          type: elementType,
          value: $(inlineElems[index]).html() !== undefined ? $(inlineElems[index]).html() : $(inlineElems[index]).text(),
          classList: $(inlineElems[index]).attr('class') !== undefined ? $(inlineElems[index]).attr('class') : undefined,
          link:
            $(inlineElems[index]).attr('data-link') !== null
              ? $(inlineElems[index]).attr('data-link')
              : $(inlineElems[index]).attr('href') !== undefined
              ? $(inlineElems[index]).attr('href')
              : undefined,
          src: $(inlineElems[index]).attr('src') !== undefined ? $(inlineElems[index]).attr('src') : undefined,
          src_compressed:
            $(inlineElems[index]).attr('src_compressed') !== null
              ? $(inlineElems[index]).attr('src_compressed')
              : $(inlineElems[index]).attr('src') !== undefined
              ? $(inlineElems[index]).attr('src')
              : undefined,
          alt: $(inlineElems[index]).attr('alt') !== undefined ? $(inlineElems[index]).attr('alt') : undefined,
          styling: $(inlineElems[index]).attr('style'),
          genericData: $(inlineElems[index]).attr('data-generic') !== null ? $(inlineElems[index]).attr('data-generic') : undefined
        };

        const newElem = this.domService.appendInteractiveElement(componentName, elementType, elemData, 'server');
        $(inlineElems[index]).replaceWith(newElem.outerHTML);
      });
    }

    return $;
  }
}

