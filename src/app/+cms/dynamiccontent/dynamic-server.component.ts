/* eslint-disable class-methods-use-this */
import { Component, Input, OnInit, ViewEncapsulation, ChangeDetectorRef, PLATFORM_ID, Inject } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle } from '@angular/platform-browser';

import { MaterialElevationDirective } from '../../shared/directives/elevate.directive';

import { InteractiveData } from './interactive.model';

import { Store } from '@ngrx/store';
import { State } from '../../store/state';

import {
  TopCategories,
  MostPopularTech,
  RecentlyUpdatedTech,
  SearchResultsCollection
} from '../../library/search/models/search-results.model';
import * as SearchAction from '../../store/search/search.actions';
import { isPlatformBrowser, ViewportScroller, isPlatformServer } from '@angular/common';

@Component({
  selector: 'app-dynamic-server',
  templateUrl: './dynamic-server.component.html',
  styleUrls: ['./dynamic.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DynamicServerComponent implements OnInit {
  // @Input() type:any;
  // @Input() entryId:any;
  // @Input() imageSource:any;
  // @Input() textData:any;
  topCategories: TopCategories;
  recentlyUpdatedTech: RecentlyUpdatedTech;
  mostPopularTech: MostPopularTech;
  latestAddedTech: SearchResultsCollection;
  latestAddedRes: SearchResultsCollection;
  recentlyUpdatedRes: SearchResultsCollection;
  latestAddedProjects: SearchResultsCollection;
  latestAddedOrganizations: SearchResultsCollection;
  showAllCategories = false;
  showAllRecents = false;
  showAllPopulars = false;
  showAllLatestAdded = false;
  showAllLatestAddedRes = false;
  showAllLatestAddedProjects = false;
  showAllLatestAddedOrganizations = false;
  showAllResRecents = false;
  @Input() elemData: InteractiveData;
  cssClasses = '';
  isExternal = false;
  isBrowser = isPlatformBrowser(this.platformId);
  showBecomeProviderBanner = false;
  showRegisterAndBecomeProviderBanner = true;
  showAppliedForProviderBanner = false;
  idToken: any;
  isAuthenticated: boolean = false;

  entry = '';
  entryDate = '';
  imageLink = '';
  entryCount = 0;
  multimediaCount = 0;

  sanitizedStyle: SafeStyle = '';
  sanitizedHtml: SafeHtml = '';

  constructor(
    private readonly sanitization: DomSanitizer,
    private readonly appStore: Store<State>,
    @Inject(PLATFORM_ID) private readonly platformId: any
  ) {}

  // Pārbauda, vai skaitli paskaidrojošam vārdam jābūt daudzskaitlī
  static plural(val: number): boolean {
    let _val = val;
    if (_val === undefined) {
      _val = 0;
    }
    if (_val % 10 === 1 && _val % 100 !== 11) {
      return false;
    }

    return true;
  }

  getServiceUrl(entity_type, resource_type, id): string {
    let url = '';
    switch (entity_type) {
      case 'Project':
        url = `/catalogue/project/${id}`;
        break;
      case 'Organization':
        url = `/catalogue/organization/${id}`;
        break;
      default:
        switch (resource_type) {
          case 'Tool/Service':
            url = `/catalogue/tool-service/${id}`;
            break;
          case 'Corpus':
            url = `/catalogue/corpus/${id}`;
            break;
          case 'Lexical/Conceptual resource':
            url = `/catalogue/lcr/${id}`;
            break;
          default:
            break;
        }
    }
    return url;
  }

  validURL(str: string): boolean {
    const pattern = new RegExp(
      '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$',
      'i'
    ); // fragment locator

    return !!pattern.test(str);
  }

  ngOnInit(): void {
    // console.log("this is spartaaaa", this.elemData);

    this.showRegisterAndBecomeProviderBanner = true;

    if (this.elemData.link !== undefined) {
      this.entry = this.elemData.link;
    }
    this.isExternal = this.validURL(this.elemData.link);

    switch (this.elemData.type) {
      case 'elg-link':
        if (this.elemData.styling && this.elemData.styling.cssText) {
          this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        } else {
          this.sanitizedStyle = '';
        }
        break;
      case 'photo-modal':
        if (this.elemData.styling && this.elemData.styling.cssText) {
          this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        } else {
          this.sanitizedStyle = '';
        }
        break;
      case 'elg-catalogue-link':
        if (this.elemData.styling && this.elemData.styling.cssText) {
          this.sanitizedStyle = this.sanitization.bypassSecurityTrustStyle(`${this.elemData.styling.cssText}`);
        } else {
          this.sanitizedStyle = '';
        }
        break;
      case 'topic-box':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'topic-box-catalogue':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section-unregistered':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'become-provider-section-applied':
        this.sanitizedHtml = this.sanitization.bypassSecurityTrustHtml(this.elemData.value);
        break;
      case 'widget-top-categories':
        this.appStore.dispatch(new SearchAction.GetTopCategories());
        this.appStore
          .select((appState: any) => appState.catalogue.topCategories)
          .subscribe((_topCategories: any) => {
            this.topCategories = _topCategories.categories;
            // this.cd.markForCheck();
          });
        break;
      case 'widget-recently-updated':
        this.appStore.dispatch(new SearchAction.GetRecentlyUpdated());
        this.appStore
          .select((appState: any) => appState.catalogue.recentlyUpdatedTech)
          .subscribe((_recentlyUpdatedTech: any) => {
            if (_recentlyUpdatedTech && _recentlyUpdatedTech.results && _recentlyUpdatedTech.results.length !== undefined) {
              this.recentlyUpdatedTech = _recentlyUpdatedTech.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-most-popular':
        this.appStore.dispatch(new SearchAction.GetMostPopular());
        this.appStore
          .select((appState: any) => appState.catalogue.mostPopularTech)
          .subscribe((_mostPopularTech: any) => {
            if (_mostPopularTech && _mostPopularTech.results && _mostPopularTech.results.length !== undefined) {
              this.mostPopularTech = _mostPopularTech.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-latest-added':
        this.appStore.dispatch(new SearchAction.GetLastAdded());
        this.appStore
          .select((appState: any) => appState.catalogue.latestAddedTech)
          .subscribe((_latestAddedTech: any) => {
            if (_latestAddedTech && _latestAddedTech.results && _latestAddedTech.results.length !== undefined) {
              this.latestAddedTech = _latestAddedTech.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-latest-added-resources':
        this.appStore.dispatch(new SearchAction.GetLastAddedResource());
        this.appStore
          .select((appState: any) => appState.catalogue.latestAddedRes)
          .subscribe((_latestAddedRes: any) => {
            if (_latestAddedRes && _latestAddedRes.results && _latestAddedRes.results.length !== undefined) {
              this.latestAddedRes = _latestAddedRes.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-recently-updated-resources':
        this.appStore.dispatch(new SearchAction.GetRecentlyUpdatedResource());
        this.appStore
          .select((appState: any) => appState.catalogue.recentlyUpdatedRes)
          .subscribe((_recentlyUpdatedRes: any) => {
            if (_recentlyUpdatedRes && _recentlyUpdatedRes.results && _recentlyUpdatedRes.results.length !== undefined) {
              this.recentlyUpdatedRes = _recentlyUpdatedRes.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-latest-added-projects':
        this.appStore.dispatch(new SearchAction.GetLastAddedProjects());
        this.appStore
          .select((appState: any) => appState.catalogue.latestAddedProjects)
          .subscribe((_latestAddedProjects: any) => {
            if (_latestAddedProjects && _latestAddedProjects.results && _latestAddedProjects.results.length !== undefined) {
              this.latestAddedProjects = _latestAddedProjects.results;
              // this.cd.markForCheck();
            }
          });
        break;
      case 'widget-latest-added-organizations':
        this.appStore.dispatch(new SearchAction.GetLastAddedOrganizations());
        this.appStore
          .select((appState: any) => appState.catalogue.latestAddedOrganizations)
          .subscribe((_latestAddedOrganizations: any) => {
            if (_latestAddedOrganizations && _latestAddedOrganizations.results && _latestAddedOrganizations.results.length !== undefined) {
              this.latestAddedOrganizations = _latestAddedOrganizations.results;
              // this.cd.markForCheck();
            }
          });
        break;
      default:
        break;
    }
  }

  toggleShowAllCategories($event): void {
    $event.preventDefault();
    this.showAllCategories = !this.showAllCategories;
  }

  toggleShowAllPopular($event): void {
    $event.preventDefault();
    this.showAllPopulars = !this.showAllPopulars;
  }

  toggleShowAllRecent($event): void {
    $event.preventDefault();
    this.showAllRecents = !this.showAllRecents;
  }

  toggleShowAllResRecent($event): void {
    $event.preventDefault();
    this.showAllResRecents = !this.showAllResRecents;
  }

  toggleShowAllLatestAdded($event): void {
    $event.preventDefault();
    this.showAllLatestAdded = !this.showAllLatestAdded;
  }

  toggleShowAllLatestAddedRes($event): void {
    $event.preventDefault();
    this.showAllLatestAddedRes = !this.showAllLatestAddedRes;
  }

  toggleShowAllLatestAddedProjects($event): void {
    $event.preventDefault();
    this.showAllLatestAddedProjects = !this.showAllLatestAddedProjects;
  }

  toggleShowAllLatestAddedOrganizations($event): void {
    $event.preventDefault();
    this.showAllLatestAddedOrganizations = !this.showAllLatestAddedOrganizations;
  }

  navigate($event, url: string): void {
    $event.preventDefault();
    if (this.isBrowser) {
      window.location.href = url;
    }
  }

  navigateService(resource_type, id): void {
    let url = '';
    switch (resource_type) {
      case 'Tool/Service':
        url = `/catalogue/resource/service/tool/${id}`;
        break;
      case 'Corpus':
        url = `/catalogue/resource/service/corpus/${id}`;
        break;
      default:
        break;
    }
    // $event.preventDefault();
    if (this.isBrowser) {
      window.location.href = url;
    }
  }

  scrollToAnchor(anchor: string): void {
    if (isPlatformServer(this.platformId)) {
      return;
    }
    const el = document.getElementById(anchor);
    el.scrollIntoView({ behavior: 'smooth' });
  }

  hasClass(classList: DOMTokenList | string | null, classNeedle: string): boolean {
    if (classList === undefined) {
      return false;
    }

    if (Array.isArray(classList)) {
      return this.elemData.classList.contains(classNeedle);
    }

    return (classList as string).includes(classNeedle);
  }

  encodeKey(key: string): string {
    const temKey = key;

    return encodeURIComponent(temKey);
  }
}

