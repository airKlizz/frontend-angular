import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../framework/material';

import { DirectivesModule } from '../../shared/directives/directives.module';

import { routes } from './../cms.routes';
import { CmsService } from './../cms.service';
import { DynamicComponent } from './../dynamiccontent/dynamic.component';
import { DynamicServerComponent } from './../dynamiccontent/dynamic-server.component';
import { DynamicServerService } from './../dynamiccontent/dynamic-server.service';
import { DynamicService } from './dynamic.service';
import { MatCardModule } from '@angular/material/card';
import { ProviderApplicantService } from '../../request-provider/provider-applicant.service';


@NgModule({
  imports: [CommonModule,
    RouterModule,
    MatCardModule,
    MaterialModule,
    DirectivesModule
  ],
  providers: [
    CmsService,
    DynamicService,
    DynamicServerService,
    ProviderApplicantService
  ],
  declarations: [
    DynamicComponent,
    DynamicServerComponent
  ],
  exports: [DynamicComponent, DynamicServerComponent],
  entryComponents: []
})

export class DynamicModule {
  static forRoot(): ModuleWithProviders<DynamicModule> {
    return {
      ngModule: DynamicModule
    }
  }
}
